#include "ScriptPCH.h"
#include "Chat.h"
#include "WorldSession.h"
#include "AccountMgr.h"
#include "Config.h"
#include "Field.h"

using namespace std;

bool CheckHealer(uint64 playerguid)
{
	Player* player = sObjectAccessor->FindPlayer(playerguid);

	if (!player)
		return false;

	switch (player->GetActiveSpecialization())
	{
	case SPEC_DRUID_RESTORATION:
	case SPEC_PRIEST_HOLY:
	case SPEC_PRIEST_DISCIPLINE:
	case SPEC_SHAMAN_RESTORATION:
	case SPEC_MONK_MISTWEAVER:
	case SPEC_PALADIN_HOLY:
		return true;
		break;
	default:
		return false;
		break;
	}
	return false;
}

bool IsSameFaction(Player* player1, Player* player2)
{
	if (player1->getFaction() == player2->getFaction())
	{
		return true;
	}
	return false;
}

bool IsAlliance(uint32 guid)
{
	if (CharacterInfo const* characterInfo = sWorld->GetCharacterInfo(guid))
	{
		if (Player* player = ObjectAccessor::FindPlayerByNameInOrOutOfWorld(characterInfo->Name.c_str()))
		{
			if (!player)
				return false;

			if (player->getFaction() == 1)
				return true;
		}
	}
	return false;
}

bool IsLeaderOfAGroup(Player* player)
{
	if (!player)
		return false;

	Group* group = player->GetGroup();
	if (!group)
		return false;

	if (group->IsLeader(player->GetGUID()))
	{
		QueryResult result = WorldDatabase.Query("SELECT playerGUID from groupfinder_groups");
		if (!result)
		{
			return false;
		}
		do{
			Field* fields = result->Fetch();
			uint32 playerguid = fields[0].GetUInt32();
			if (playerguid == uint32(player->GetGUID()))
			{
				return true;
			}
		} while (result->NextRow());
	}
	return false;
}

class group_finder : public CreatureScript
{
private:
	uint32 pandawow = 1000;
	uint32 pandawow2 = -1000;
public:
	group_finder() : CreatureScript("group_finder"){ }

	struct group_finderAI : public ScriptedAI
	{
		group_finderAI(Creature *c) : ScriptedAI(c){ }
	};
	bool wantsHealers = false;
	CreatureAI* GetAI(Creature* _creature) const
	{
		return new group_finderAI(_creature);
	}

	bool OnGossipHello(Player* player, Creature* creature)
	{
		player->PlayerTalkClass->ClearMenus();
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Find Players", GOSSIP_SENDER_MAIN, 1);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Find Groups", GOSSIP_SENDER_MAIN, 2);
		player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "Sign-up", GOSSIP_SENDER_MAIN, 3, "Please write your experience in Arena.", 0, true);
		player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "Sign-up group", GOSSIP_SENDER_MAIN, 4, "Please write your team details.", 0, true);
		if (IsLeaderOfAGroup(player))
		{
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "View your group listing", GOSSIP_SENDER_MAIN, 5);
		}

		wantsHealers = false;
		player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
		return true;
	}

	bool onGossipListGroupSignUps(Player* player, Creature* creature)
	{
		player->PlayerTalkClass->ClearMenus();
		QueryResult result = WorldDatabase.Query("SELECT leaderguid, playerGUID from groupfinder_signups");
		if (!result)
			return true;
		do{
			Field* fields = result->Fetch();
			uint32 leaderguid = fields[0].GetUInt32();
			uint32 playerguid = fields[1].GetUInt32();
			if (leaderguid == uint32(player->GetGUID()))
			{
				std::stringstream ss;
				if (CharacterInfo const* characterInfo = sWorld->GetCharacterInfo(playerguid))
				{
					switch (characterInfo->Class){
					case 1:
						ss << "[Name: " << characterInfo->Name << "] - [Class: Warrior]";
						break;
					case 2:
						ss << "[Name: " << characterInfo->Name << "] - [Class: Paladin]";
						break;
					case 3:
						ss << "[Name: " << characterInfo->Name << "] - [Class: Hunter]";
						break;
					case 4:
						ss << "[Name: " << characterInfo->Name << "] - [Class: Rogue]";
						break;
					case 5:
						ss << "[Name: " << characterInfo->Name << "] - [Class: Priest]";
						break;
					case 7:
						ss << "[Name: " << characterInfo->Name << "] - [Class: Shaman]";
						break;
					case 8:
						ss << "[Name: " << characterInfo->Name << "] - [Class: Mage]";
						break;
					case 9:
						ss << "[Name: " << characterInfo->Name << "] - [Class: Warlock]";
						break;
					case 10:
						ss << "[Name: " << characterInfo->Name << "] - [Class: Monk]";
						break;
					case 11:
						ss << "[Name: " << characterInfo->Name << "] - [Class: Druid]";
						break;
					case 6:
						ss << "[Name: " << characterInfo->Name << "] - [Class: Death Knight]";
						break;
					}
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss.str(), GOSSIP_SENDER_MAIN, -100000 - playerguid);
				}
			}
		} while (result->NextRow());
		player->PlayerTalkClass->SendGossipMenu(creature->GetEntry(), creature->GetGUID());
		return true;
	}

	bool OnGossipSelectCode(Player* player, Creature* creature, uint32 /* sender */, uint32 actions, const char* code)
	{
		player->PlayerTalkClass->ClearMenus();
		if (actions == 3)
		{
			std::string details = code;
			if (details.empty())
			{
				details = "NONE";
			}
			if (details.length() > 50)
			{
				player->CLOSE_GOSSIP_MENU();
				ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Information]:|r Character limit cannot exceed 50!|r!");
				return false;
			}
			QueryResult result = WorldDatabase.Query("SELECT playerGUID from groupfinder_players");
			if (!result)
			{
				player->CLOSE_GOSSIP_MENU();
				WorldDatabase.PExecute("INSERT INTO groupfinder_players (playerGUID, playerDetails) VALUES (%u, 'NONE')", player->GetGUID());
				WorldDatabase.PExecute("UPDATE groupfinder_players SET playerDetails='%s' WHERE playerGUID='%u'", details.c_str(), player->GetGUID());
				ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Information]:|r You have been signed up on the Groupfinder!|r!");
				return true;
			}
			do{
				Field* fields = result->Fetch();
				uint32 playerguid1 = fields[0].GetInt32();
				uint32 playerguid2 = player->GetGUID();
				if (playerguid1 == playerguid2)
				{
					ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Information]:|r You have already been signed up on the Groupfinder!|r!");
					OnGossipHello(player, creature);
					return false;
				}
			} while (result->NextRow());
			player->CLOSE_GOSSIP_MENU();
			WorldDatabase.PExecute("INSERT INTO groupfinder_players (playerGUID, playerDetails) VALUES (%u, 'NONE')", player->GetGUID());
			WorldDatabase.PExecute("UPDATE groupfinder_players SET playerDetails='%s' WHERE playerGUID='%u'", details.c_str(), player->GetGUID());
			ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Information]:|r You have been signed up on the Groupfinder!|r!");
			return true;
		}
		else if (actions == 4)
		{
			Group* group = player->GetGroup();
			if (group)
			{
				if (group->IsLeader(player->GetGUID()))
				{
					player->PlayerTalkClass->ClearMenus();
					std::string details = code;
					if (details.empty())
					{
						details = "NONE";
					}
					if (details.length() > 50)
					{
						player->CLOSE_GOSSIP_MENU();
						ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Information]:|r Character limit cannot exceed 50!|r!");
						return false;
					}
					QueryResult result = WorldDatabase.Query("SELECT playerGUID from groupfinder_groups");
					if (!result)
					{
						player->CLOSE_GOSSIP_MENU();
						WorldDatabase.PExecute("INSERT INTO groupfinder_groups (playerGUID, playerDetails) VALUES (%u, 'NONE')", player->GetGUID());
						WorldDatabase.PExecute("UPDATE groupfinder_groups SET playerDetails='%s' WHERE playerGUID='%u'", details.c_str(), player->GetGUID());
						ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Information]:|r Your group has been signed up on the Groupfinder!|r!");
						return true;
					}
					do{
						Field* fields = result->Fetch();
						uint32 playerguid1 = fields[0].GetInt32();
						uint32 playerguid2 = player->GetGUID();
						if (playerguid1 == playerguid2)
						{
							ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Information]:|r Your group has already been signed up on the Groupfinder!|r!");
							OnGossipHello(player, creature);
							return false;
						}
					} while (result->NextRow());
					player->CLOSE_GOSSIP_MENU();
					WorldDatabase.PExecute("INSERT INTO groupfinder_groups (playerGUID, playerDetails) VALUES (%u, 'NONE')", player->GetGUID());
					WorldDatabase.PExecute("INSERT INTO groupfinder_groups (playerGUID, playerDetails) VALUES (%u, '%s')", player->GetGUID(), details.c_str());
					ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Information]:|r Your group has been signed up on the Groupfinder!|r!");
					return true;
				}
				else
				{
					ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Information]:|r Only the group leader can sign up!|r!");
					OnGossipHello(player, creature);
					return false;
				}
			}
			else
			{
				ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Information]:|r You are not in a group!|r!");
				OnGossipHello(player, creature);
				return false;
			}
		}
		return false;
	}

	bool onGossipSignUpForGroup(Player* player, Creature* creature, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();
		uint32 leaderguid = uint32(abs(int(action + 10000)));
		if (player->GetGUID() == leaderguid)
		{
			ChatHandler(player->GetSession()).PSendSysMessage("ERROR: You cannot sign up for your own group!");
			player->CLOSE_GOSSIP_MENU();
			return false;
		}
		QueryResult result = WorldDatabase.Query("SELECT leaderGUID, playerGUID from groupfinder_signups");
		if (!result)
		{
			player->CLOSE_GOSSIP_MENU();
			WorldDatabase.PExecute("INSERT INTO groupfinder_signups (leaderGUID, playerGUID) VALUES (%u, %u)", leaderguid, player->GetGUID());
			if (CharacterInfo const* characterInfo = sWorld->GetCharacterInfo(leaderguid))
			{
				if (Player* leader = ObjectAccessor::FindPlayerByNameInOrOutOfWorld(characterInfo->Name.c_str()))
				{
					if (IsSameFaction(player, leader))
						ChatHandler(leader->GetSession()).PSendSysMessage("|cffff6060[Information]:|r A player has signed up to your group!|r!");
					else
					{
						ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Information]:|r You are not the same faction as the leader of that group!|r!");
						player->CLOSE_GOSSIP_MENU();
						return false;
					}
				}
			}
			ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Information]:|r You have been signed up for that group!|r!");
			return true;
		}
		do{
			Field* fields = result->Fetch();
			uint32 playerguid1 = fields[1].GetInt32();
			uint32 playerguid2 = player->GetGUID();
			if (playerguid1 == playerguid2)
			{
				ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Information]:|r You are already signed up for that group!|r!");
				OnGossipHello(player, creature);
				return false;
			}
		} while (result->NextRow());
		player->CLOSE_GOSSIP_MENU();
		if (CharacterInfo const* characterInfo = sWorld->GetCharacterInfo(leaderguid))
		{
			if (Player* leader = ObjectAccessor::FindPlayerByNameInOrOutOfWorld(characterInfo->Name.c_str()))
			{
				if (IsSameFaction(player, leader))
					ChatHandler(leader->GetSession()).PSendSysMessage("|cffff6060[Information]:|r A player has signed up to your group!|r!");
				else
				{
					ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Information]:|r You are not the same faction as the leader of that group!|r!");
					player->CLOSE_GOSSIP_MENU();
					return false;
				}
			}
		}
		WorldDatabase.PExecute("INSERT INTO groupfinder_signups (leaderGUID, playerGUID) VALUES (%u, %u)", leaderguid, player->GetGUID());
		ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Information]:|r You have been signed up for that group!|r!");
		return true;
	}

	bool onGossipDPSOrHealer(Player* player, Creature* creature)
	{
		player->PlayerTalkClass->ClearMenus();
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "DPS", GOSSIP_SENDER_MAIN, 10);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Healer", GOSSIP_SENDER_MAIN, 11);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "[Back]", GOSSIP_SENDER_MAIN, -10002);
		player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
		return true;
	}

	bool onGossipListGroups(Player* player, Creature* creature)
	{
		QueryResult result = WorldDatabase.Query("SELECT playerGUID, playerDetails from groupfinder_groups");
		Field* fields = NULL;
		if (!result)
		{
			ChatHandler(player->GetSession()).PSendSysMessage("There are no groups listed in the Groupfinder.");
			player->CLOSE_GOSSIP_MENU();
			return false;
		}
		player->PlayerTalkClass->ClearMenus();
		do {
			Field* fields = result->Fetch();
			uint64 playerguid = fields[0].GetUInt32();
			std::string playerDetails = fields[1].GetString();
			std::stringstream ss;
			if (IsAlliance(playerguid))
			{
				if (IsAlliance(uint32(player->GetGUID())))
				{
					ss << "[" << playerDetails << "]";
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss.str(), GOSSIP_SENDER_MAIN, pandawow2 - playerguid);
				}
			}
			else
			{
				if (!IsAlliance(uint32(player->GetGUID())))
				{
					ss << "[" << playerDetails << "]";
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss.str(), GOSSIP_SENDER_MAIN, pandawow2 - playerguid);
				}
			}
		} while (result->NextRow());
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "[Back]", GOSSIP_SENDER_MAIN, -10003);
		player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
		return true;
	}

	bool onGossipListPlayers(Player* player, Creature* creature, bool healers)
	{
		wantsHealers = healers;
		QueryResult result = WorldDatabase.Query("SELECT playerGUID from groupfinder_players");
		Field * fields = NULL;
		if (!result)
		{
			ChatHandler(player->GetSession()).PSendSysMessage("There are no players listed in the Groupfinder.");
			player->CLOSE_GOSSIP_MENU();
			return false;
		}
		player->PlayerTalkClass->ClearMenus();
		do {
			Field* fields = result->Fetch();
			uint64 playerguid = fields[0].GetUInt64();
			std::stringstream ss;
			if (healers == false)
			{
				if (CheckHealer(playerguid) == false)
				{
					if (CharacterInfo const* characterInfo = sWorld->GetCharacterInfo(playerguid))
					{
						if (IsAlliance(playerguid))
						{
							if (IsAlliance(uint32(player->GetGUID())))
							{
								switch (characterInfo->Class){
								case 1:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Warrior]";
									break;
								case 2:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Paladin]";
									break;
								case 3:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Hunter]";
									break;
								case 4:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Rogue]";
									break;
								case 5:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Priest]";
									break;
								case 7:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Shaman]";
									break;
								case 8:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Mage]";
									break;
								case 9:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Warlock]";
									break;
								case 10:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Monk]";
									break;
								case 11:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Druid]";
									break;
								case 6:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Death Knight]";
									break;
								}
							}
						}
						else
						{
							if (!IsAlliance(uint32(player->GetGUID())))
							{
								switch (characterInfo->Class){
								case 1:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Warrior]";
									break;
								case 2:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Paladin]";
									break;
								case 3:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Hunter]";
									break;
								case 4:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Rogue]";
									break;
								case 5:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Priest]";
									break;
								case 7:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Shaman]";
									break;
								case 8:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Mage]";
									break;
								case 9:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Warlock]";
									break;
								case 10:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Monk]";
									break;
								case 11:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Druid]";
									break;
								case 6:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Death Knight]";
									break;
								}
							}
						}
					}
				}
			}
			else
			{
				if (CheckHealer(playerguid))
				{
					if (CharacterInfo const* characterInfo = sWorld->GetCharacterInfo(playerguid))
					{
						if (IsAlliance(playerguid))
						{
							if (IsAlliance(uint32(player->GetGUID())))
							{
								switch (characterInfo->Class){
								case 1:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Warrior]";
									break;
								case 2:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Paladin]";
									break;
								case 3:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Hunter]";
									break;
								case 4:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Rogue]";
									break;
								case 5:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Priest]";
									break;
								case 7:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Shaman]";
									break;
								case 8:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Mage]";
									break;
								case 9:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Warlock]";
									break;
								case 10:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Monk]";
									break;
								case 11:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Druid]";
									break;
								case 6:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Death Knight]";
									break;
								}
							}
						}
						else
						{
							if (!IsAlliance(uint32(player->GetGUID())))
							{
								switch (characterInfo->Class){
								case 1:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Warrior]";
									break;
								case 2:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Paladin]";
									break;
								case 3:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Hunter]";
									break;
								case 4:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Rogue]";
									break;
								case 5:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Priest]";
									break;
								case 7:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Shaman]";
									break;
								case 8:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Mage]";
									break;
								case 9:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Warlock]";
									break;
								case 10:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Monk]";
									break;
								case 11:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Druid]";
									break;
								case 6:
									ss << "[Name: " << characterInfo->Name << "] - [Class: Death Knight]";
									break;
								}
							}
						}
					}
				}
			}
			if (ss.str() != "")
			    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss.str(), GOSSIP_SENDER_MAIN, pandawow + playerguid);
			else
			{
				ChatHandler(player->GetSession()).PSendSysMessage("No players are currently listed!");
				onGossipDPSOrHealer(player, creature);
				return false;
			}
		} while (result->NextRow());
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "[Back]", GOSSIP_SENDER_MAIN, -10001);
		player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
		return true;
	}

	bool onGossipListGroupDetails(Player* player, Creature* creature, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();
		uint32 leaderguid = uint32(abs(int(action + 1000)));
		if (CharacterInfo const* characterInfo = sWorld->GetCharacterInfo(leaderguid))
		{
			Player* leader = ObjectAccessor::FindPlayerByNameInOrOutOfWorld(characterInfo->Name.c_str());

			if (!leader)
			{
				ChatHandler(player->GetSession()).PSendSysMessage("ERROR: Problem when finding leader of group.");
				player->CLOSE_GOSSIP_MENU();
				return false;
			}
			Group* group = leader->GetGroup();

			if (!group)
			{
				ChatHandler(player->GetSession()).PSendSysMessage("ERROR: That player's group has been disbanded.");
				player->CLOSE_GOSSIP_MENU();
				return false;
			}
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "[Sign-up to group]", GOSSIP_SENDER_MAIN, 10000 + leaderguid);
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "[Back]", GOSSIP_SENDER_MAIN, -10004);
			player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
			return true;
		}
		return false;
	}

	bool onGossipListPlayerDetails(Player* player, Creature* creature, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();
		uint32 playerguid = action - 1000;
		std::stringstream ss0;
		std::stringstream ss1;
		std::stringstream ss2;
		std::stringstream ss3;
		std::stringstream spec;
		std::stringstream ilvl;
		std::string details;
		QueryResult result = CharacterDatabase.Query("SELECT guid, rating0, rating1, rating2 FROM character_arena_data");
		QueryResult result2 = CharacterDatabase.Query("SELECT guid, class, activespec, specialization1, specialization2 FROM characters");
		QueryResult result3 = WorldDatabase.Query("SELECT playerGUID, playerDetails FROM groupfinder_players");
		if (!result || !result2 || !result3)
		{
			ChatHandler(player->GetSession()).PSendSysMessage("ERROR: Something went wrong while grabbing rating from player.");
			player->CLOSE_GOSSIP_MENU();
			return false;
		}
		else if (CharacterInfo const* characterInfo = sWorld->GetCharacterInfo(playerguid))
		{
			ss0 << "[Showing results for Player: " << characterInfo->Name;
			if (Player* playerobj = ObjectAccessor::FindPlayerByNameInOrOutOfWorld(characterInfo->Name.c_str()))
			{
				ilvl << "Current Item Level: [" << playerobj->GetAverageItemLevel();
				ilvl << "]";
			}
			else
			{
				ilvl << "Current Item Level: [UNKNOWN]";
			}
		}
		do{
			Field* fields3 = result3->Fetch();
			if (fields3[0].GetInt32() == playerguid)
			{
				details = "[Player-written details]: " + fields3[1].GetString();
			}
		} while (result3->NextRow());
		ss0 << "]";
		do{
			Field* fields = result->Fetch();
			if (fields[0].GetInt32() == playerguid)
			{
				uint32 rating2v2 = fields[1].GetInt32();
				uint32 rating3v3 = fields[2].GetInt32();
				uint32 rating1v1 = fields[3].GetInt32();
				ss1 << "[Rating - 1v1]: " << rating1v1;
				ss2 << "[Rating - 2v2]: " << rating2v2;
				ss3 << "[Rating - 3v3]: " << rating3v3;
			}
		} while (result->NextRow());
		do{
			Field* fields2 = result2->Fetch();
			if (fields2[0].GetInt32() == playerguid)
			{
				uint32 uclass = fields2[1].GetInt32();
				uint32 activespec = fields2[2].GetInt32();
				uint32 spec1 = fields2[3].GetInt32();
				uint32 spec2 = fields2[4].GetInt32();
				if (activespec == 0)
				{
					switch (uclass){
					case 1:
						if (spec1 == 71)
							spec << "[Specialization]: Arms";
						else if (spec1 == 72)
							spec << "[Specialization]: Fury";
						else if (spec1 == 73)
							spec << "[Specialization]: Protection";
						break;
					case 2:
						if (spec1 == 65)
							spec << "[Specialization]: Holy";
						else if (spec1 == 66)
							spec << "[Specialization]: Protection";
						else if (spec1 == 70)
							spec << "[Specialization]: Retribution";
						break;
					case 3:
						if (spec1 == 253)
							spec << "[Specialization]: Beast Mastery";
						else if (spec1 == 254)
							spec << "[Specialization]: Marksmanship";
						else if (spec1 == 255)
							spec << "[Specialization]: Survival";
						break;
					case 4:
						if (spec1 == 259)
							spec << "[Specialization]: Assassination";
						else if (spec1 == 260)
							spec << "[Specialization]: Combat";
						else if (spec1 == 261)
							spec << "[Specialization]: Subtlety";
						break;
					case 5:
						if (spec1 == 256)
							spec << "[Specialization]: Discipline";
						else if (spec1 == 257)
							spec << "[Specialization]: Holy";
						else if (spec1 == 258)
							spec << "[Specialization]: Shadow";
						break;
					case 7:
						if (spec1 == 262)
							spec << "[Specialization]: Elemental";
						else if (spec1 == 263)
							spec << "[Specialization]: Enhancement";
						else if (spec1 == 264)
							spec << "[Specialization]: Restoration";
						break;
					case 8:
						if (spec1 == 62)
							spec << "[Specialization]: Arcane";
						else if (spec1 == 63)
							spec << "[Specialization]: Fire";
						else if (spec1 == 64)
							spec << "[Specialization]: Frost";
						break;
					case 9:
						if (spec1 == 265)
							spec << "[Specialization]: Affiliction";
						else if (spec1 == 266)
							spec << "[Specialization]: Demonology";
						else if (spec1 == 267)
							spec << "[Specialization]: Destruction";
						break;
					case 10:
						if (spec1 == 268)
							spec << "[Specialization]: Brewmaster";
						else if (spec1 == 270)
							spec << "[Specialization]: Mistweaver";
						else if (spec1 == 269)
							spec << "[Specialization]: Windwalker";
						break;
					case 11:
						if (spec1 == 102)
							spec << "[Specialization]: Balance";
						else if (spec1 == 103)
							spec << "[Specialization]: Feral";
						else if (spec1 == 104)
							spec << "[Specialization]: Guardian";
						else if (spec1 == 105)
							spec << "[Specialization]: Restoration";
						break;
					case 6:
						if (spec1 == 250)
							spec << "[Specialization]: Blood";
						else if (spec1 == 251)
							spec << "[Specialization]: Frost";
						else if (spec1 == 252)
							spec << "[Specialization]: Unholy";
						break;
					}
				}
				else 
				{
					if (activespec == 1)
					{
						switch (uclass){
						case 1:
							if (spec1 == 71)
								spec << "[Specialization]: Arms";
							else if (spec1 == 72)
								spec << "[Specialization]: Fury";
							else if (spec1 == 73)
								spec << "[Specialization]: Protection";
							break;
						case 2:
							if (spec1 == 65)
								spec << "[Specialization]: Holy";
							else if (spec1 == 66)
								spec << "[Specialization]: Protection";
							else if (spec1 == 70)
								spec << "[Specialization]: Retribution";
							break;
						case 3:
							if (spec1 == 253)
								spec << "[Specialization]: Beast Mastery";
							else if (spec1 == 254)
								spec << "[Specialization]: Marksmanship";
							else if (spec1 == 255)
								spec << "[Specialization]: Survival";
							break;
						case 4:
							if (spec1 == 259)
								spec << "[Specialization]: Assassination";
							else if (spec1 == 260)
								spec << "[Specialization]: Combat";
							else if (spec1 == 261)
								spec << "[Specialization]: Subtlety";
							break;
						case 5:
							if (spec1 == 256)
								spec << "[Specialization]: Discipline";
							else if (spec1 == 257)
								spec << "[Specialization]: Holy";
							else if (spec1 == 258)
								spec << "[Specialization]: Shadow";
							break;
						case 7:
							if (spec1 == 262)
								spec << "[Specialization]: Elemental";
							else if (spec1 == 263)
								spec << "[Specialization]: Enhancement";
							else if (spec1 == 264)
								spec << "[Specialization]: Restoration";
							break;
						case 8:
							if (spec1 == 62)
								spec << "[Specialization]: Arcane";
							else if (spec1 == 63)
								spec << "[Specialization]: Fire";
							else if (spec1 == 64)
								spec << "[Specialization]: Frost";
							break;
						case 9:
							if (spec1 == 265)
								spec << "[Specialization]: Affiliction";
							else if (spec1 == 266)
								spec << "[Specialization]: Demonology";
							else if (spec1 == 267)
								spec << "[Specialization]: Destruction";
							break;
						case 10:
							if (spec1 == 268) 
								spec << "[Specialization]: Brewmaster";
							else if (spec1 == 270)
								spec << "[Specialization]: Mistweaver";
							else if (spec1 == 269)
								spec << "[Specialization]: Windwalker";
							break;
						case 11:
							if (spec1 == 102)
								spec << "[Specialization]: Balance";
							else if (spec1 == 103)
								spec << "[Specialization]: Feral";
							else if (spec1 == 104)
								spec << "[Specialization]: Guardian";
							else if (spec1 == 105)
								spec << "[Specialization]: Restoration";
							break;
						case 6:
							if (spec1 == 250)
								spec << "[Specialization]: Blood";
							else if (spec1 == 251)
								spec << "[Specialization]: Frost";
							else if (spec1 == 252)
								spec << "[Specialization]: Unholy";
							break;
						}
					}
					else
					{
						switch (uclass){
						case 1:
							if (spec2 == 71)
								spec << "[Specialization]: Arms";
							else if (spec2 == 72)
								spec << "[Specialization]: Fury";
							else if (spec2 == 73)
								spec << "[Specialization]: Protection";
							break;
						case 2:
							if (spec2 == 65)
								spec << "[Specialization]: Holy";
							else if (spec2 == 66)
								spec << "[Specialization]: Protection";
							else if (spec2 == 70)
								spec << "[Specialization]: Retribution";
							break;
						case 3:
							if (spec2 == 253)
								spec << "[Specialization]: Beast Mastery";
							else if (spec2 == 254)
								spec << "[Specialization]: Marksmanship";
							else if (spec2 == 255)
								spec << "[Specialization]: Survival";
							break;
						case 4:
							if (spec2 == 259)
								spec << "[Specialization]: Assassination";
							else if (spec2 == 260)
								spec << "[Specialization]: Combat";
							else if (spec2 == 261)
								spec << "[Specialization]: Subtlety";
							break;
						case 5:
							if (spec2 == 256)
								spec << "[Specialization]: Discipline";
							else if (spec2 == 257)
								spec << "[Specialization]: Holy";
							else if (spec2 == 258)
								spec << "[Specialization]: Shadow";
							break;
						case 7:
							if (spec2 == 262)
								spec << "[Specialization]: Elemental";
							else if (spec2 == 263)
								spec << "[Specialization]: Enhancement";
							else if (spec2 == 264)
								spec << "[Specialization]: Restoration";
							break;
						case 8:
							if (spec2 == 62)
								spec << "[Specialization]: Arcane";
							else if (spec2 == 63)
								spec << "[Specialization]: Fire";
							else if (spec2 == 64)
								spec << "[Specialization]: Frost";
							break;
						case 9:
							if (spec2 == 265)
								spec << "[Specialization]: Affiliction";
							else if (spec2 == 266)
								spec << "[Specialization]: Demonology";
							else if (spec2 == 267)
								spec << "[Specialization]: Destruction";
							break;
						case 10:
							if (spec2 == 268)
								spec << "[Specialization]: Brewmaster";
							else if (spec2 == 270)
								spec << "[Specialization]: Mistweaver";
							else if (spec2 == 269)
								spec << "[Specialization]: Windwalker";
							break;
						case 11:
							if (spec2 == 102)
								spec << "[Specialization]: Balance";
							else if (spec2 == 103)
								spec << "[Specialization]: Feral";
							else if (spec2 == 104)
								spec << "[Specialization]: Guardian";
							else if (spec2 == 105)
								spec << "[Specialization]: Restoration";
							break;
						case 6:
							if (spec2 == 250)
								spec << "[Specialization]: Blood";
							else if (spec2 == 251)
								spec << "[Specialization]: Frost";
							else if (spec2 == 252)
								spec << "[Specialization]: Unholy";
							break;
						}
					}
				}
			}
		} while (result2->NextRow());
		if (spec.str() == "")
		{
			spec << "[Specialization]: UNKNOWN";
		}
		if (details == "")
		{
			spec << "[Player-written details]: NONE";
		}
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss0.str(), GOSSIP_SENDER_MAIN, 100);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, spec.str(), GOSSIP_SENDER_MAIN, 104);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss1.str(), GOSSIP_SENDER_MAIN, 101);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss2.str(), GOSSIP_SENDER_MAIN, 102);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss3.str(), GOSSIP_SENDER_MAIN, 103);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ilvl.str(), GOSSIP_SENDER_MAIN, 105);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, details, GOSSIP_SENDER_MAIN, 106);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "[Invite]", GOSSIP_SENDER_MAIN, 100000 + playerguid);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "[Back]", GOSSIP_SENDER_MAIN, -10000);
		player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
		return true;
	}

	bool onGossipListSignUpPlayerDetails(Player* player, Creature* creature, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();
		uint32 playerguid = uint32(abs(int(action + 100000)));
		std::stringstream ss0;
		std::stringstream ss1;
		std::stringstream ss2;
		std::stringstream ss3;
		std::stringstream spec;
		std::stringstream ilvl;
		std::string details;
		QueryResult result = CharacterDatabase.Query("SELECT guid, rating0, rating1, rating2 FROM character_arena_data");
		QueryResult result2 = CharacterDatabase.Query("SELECT guid, class, activespec, specialization1, specialization2 FROM characters");
		if (!result || !result2)
		{
			player->GetSession()->SendAreaTriggerMessage("ERROR: Something went wrong while grabbing rating from player.");
			onGossipListGroupSignUps(player, creature);
			return false;
		}
		if (Player* playerobj = ObjectAccessor::FindPlayer(playerguid))
		{
			ss0 << "[Showing results for Player: " << playerobj->GetName();
			ilvl << "Current Item Level: [" << playerobj->GetAverageItemLevel();
			ilvl << "]";
		}
		if (CharacterInfo const* characterInfo = sWorld->GetCharacterInfo(playerguid))
		{
			ss0 << "[Showing results for Player: " << characterInfo->Name;
			if (Player* playerobj = ObjectAccessor::FindPlayerByNameInOrOutOfWorld(characterInfo->Name.c_str()))
			{
				ilvl << "Current Item Level: [" << playerobj->GetAverageItemLevel();
				ilvl << "]";
			}
			else
			{
				ilvl << "Current Item Level: [UNKNOWN]";
			}
		}
		ss0 << "]";
		do{
			Field* fields = result->Fetch();
			if (fields[0].GetInt32() == playerguid)
			{
				uint32 rating2v2 = fields[1].GetInt32();
				uint32 rating3v3 = fields[2].GetInt32();
				uint32 rating1v1 = fields[3].GetInt32();
				ss1 << "[Rating - 1v1]: " << rating1v1;
				ss2 << "[Rating - 2v2]: " << rating2v2;
				ss3 << "[Rating - 3v3]: " << rating3v3;
			}
		} while (result->NextRow());
		do{
			Field* fields2 = result2->Fetch();
			if (fields2[0].GetInt32() == playerguid)
			{
				uint32 uclass = fields2[1].GetInt32();
				uint32 activespec = fields2[2].GetInt32();
				uint32 spec1 = fields2[3].GetInt32();
				uint32 spec2 = fields2[4].GetInt32();
				if (activespec == 0)
				{
					switch (uclass){
					case 1:
						if (spec1 == 71)
							spec << "[Specialization]: Arms";
						else if (spec1 == 72)
							spec << "[Specialization]: Fury";
						else if (spec1 == 73)
							spec << "[Specialization]: Protection";
						break;
					case 2:
						if (spec1 == 65)
							spec << "[Specialization]: Holy";
						else if (spec1 == 66)
							spec << "[Specialization]: Protection";
						else if (spec1 == 70)
							spec << "[Specialization]: Retribution";
						break;
					case 3:
						if (spec1 == 253)
							spec << "[Specialization]: Beast Mastery";
						else if (spec1 == 254)
							spec << "[Specialization]: Marksmanship";
						else if (spec1 == 255)
							spec << "[Specialization]: Survival";
						break;
					case 4:
						if (spec1 == 259)
							spec << "[Specialization]: Assassination";
						else if (spec1 == 260)
							spec << "[Specialization]: Combat";
						else if (spec1 == 261)
							spec << "[Specialization]: Subtlety";
						break;
					case 5:
						if (spec1 == 256)
							spec << "[Specialization]: Discipline";
						else if (spec1 == 257)
							spec << "[Specialization]: Holy";
						else if (spec1 == 258)
							spec << "[Specialization]: Shadow";
						break;
					case 7:
						if (spec1 == 262)
							spec << "[Specialization]: Elemental";
						else if (spec1 == 263)
							spec << "[Specialization]: Enhancement";
						else if (spec1 == 264)
							spec << "[Specialization]: Restoration";
						break;
					case 8:
						if (spec1 == 62)
							spec << "[Specialization]: Arcane";
						else if (spec1 == 63)
							spec << "[Specialization]: Fire";
						else if (spec1 == 64)
							spec << "[Specialization]: Frost";
						break;
					case 9:
						if (spec1 == 265)
							spec << "[Specialization]: Affiliction";
						else if (spec1 == 266)
							spec << "[Specialization]: Demonology";
						else if (spec1 == 267)
							spec << "[Specialization]: Destruction";
						break;
					case 10:
						if (spec1 == 268)
							spec << "[Specialization]: Brewmaster";
						else if (spec1 == 270)
							spec << "[Specialization]: Mistweaver";
						else if (spec1 == 269)
							spec << "[Specialization]: Windwalker";
						break;
					case 11:
						if (spec1 == 102)
							spec << "[Specialization]: Balance";
						else if (spec1 == 103)
							spec << "[Specialization]: Feral";
						else if (spec1 == 104)
							spec << "[Specialization]: Guardian";
						else if (spec1 == 105)
							spec << "[Specialization]: Restoration";
						break;
					case 6:
						if (spec1 == 250)
							spec << "[Specialization]: Blood";
						else if (spec1 == 251)
							spec << "[Specialization]: Frost";
						else if (spec1 == 252)
							spec << "[Specialization]: Unholy";
						break;
					}
				}
				else
				{
					if (activespec == 1)
					{
						switch (uclass){
						case 1:
							if (spec1 == 71)
								spec << "[Specialization]: Arms";
							else if (spec1 == 72)
								spec << "[Specialization]: Fury";
							else if (spec1 == 73)
								spec << "[Specialization]: Protection";
							break;
						case 2:
							if (spec1 == 65)
								spec << "[Specialization]: Holy";
							else if (spec1 == 66)
								spec << "[Specialization]: Protection";
							else if (spec1 == 70)
								spec << "[Specialization]: Retribution";
							break;
						case 3:
							if (spec1 == 253)
								spec << "[Specialization]: Beast Mastery";
							else if (spec1 == 254)
								spec << "[Specialization]: Marksmanship";
							else if (spec1 == 255)
								spec << "[Specialization]: Survival";
							break;
						case 4:
							if (spec1 == 259)
								spec << "[Specialization]: Assassination";
							else if (spec1 == 260)
								spec << "[Specialization]: Combat";
							else if (spec1 == 261)
								spec << "[Specialization]: Subtlety";
							break;
						case 5:
							if (spec1 == 256)
								spec << "[Specialization]: Discipline";
							else if (spec1 == 257)
								spec << "[Specialization]: Holy";
							else if (spec1 == 258)
								spec << "[Specialization]: Shadow";
							break;
						case 7:
							if (spec1 == 262)
								spec << "[Specialization]: Elemental";
							else if (spec1 == 263)
								spec << "[Specialization]: Enhancement";
							else if (spec1 == 264)
								spec << "[Specialization]: Restoration";
							break;
						case 8:
							if (spec1 == 62)
								spec << "[Specialization]: Arcane";
							else if (spec1 == 63)
								spec << "[Specialization]: Fire";
							else if (spec1 == 64)
								spec << "[Specialization]: Frost";
							break;
						case 9:
							if (spec1 == 265)
								spec << "[Specialization]: Affiliction";
							else if (spec1 == 266)
								spec << "[Specialization]: Demonology";
							else if (spec1 == 267)
								spec << "[Specialization]: Destruction";
							break;
						case 10:
							if (spec1 == 268)
								spec << "[Specialization]: Brewmaster";
							else if (spec1 == 270)
								spec << "[Specialization]: Mistweaver";
							else if (spec1 == 269)
								spec << "[Specialization]: Windwalker";
							break;
						case 11:
							if (spec1 == 102)
								spec << "[Specialization]: Balance";
							else if (spec1 == 103)
								spec << "[Specialization]: Feral";
							else if (spec1 == 104)
								spec << "[Specialization]: Guardian";
							else if (spec1 == 105)
								spec << "[Specialization]: Restoration";
							break;
						case 6:
							if (spec1 == 250)
								spec << "[Specialization]: Blood";
							else if (spec1 == 251)
								spec << "[Specialization]: Frost";
							else if (spec1 == 252)
								spec << "[Specialization]: Unholy";
							break;
						}
					}
					else
					{
						switch (uclass){
						case 1:
							if (spec2 == 71)
								spec << "[Specialization]: Arms";
							else if (spec2 == 72)
								spec << "[Specialization]: Fury";
							else if (spec2 == 73)
								spec << "[Specialization]: Protection";
							break;
						case 2:
							if (spec2 == 65)
								spec << "[Specialization]: Holy";
							else if (spec2 == 66)
								spec << "[Specialization]: Protection";
							else if (spec2 == 70)
								spec << "[Specialization]: Retribution";
							break;
						case 3:
							if (spec2 == 253)
								spec << "[Specialization]: Beast Mastery";
							else if (spec2 == 254)
								spec << "[Specialization]: Marksmanship";
							else if (spec2 == 255)
								spec << "[Specialization]: Survival";
							break;
						case 4:
							if (spec2 == 259)
								spec << "[Specialization]: Assassination";
							else if (spec2 == 260)
								spec << "[Specialization]: Combat";
							else if (spec2 == 261)
								spec << "[Specialization]: Subtlety";
							break;
						case 5:
							if (spec2 == 256)
								spec << "[Specialization]: Discipline";
							else if (spec2 == 257)
								spec << "[Specialization]: Holy";
							else if (spec2 == 258)
								spec << "[Specialization]: Shadow";
							break;
						case 7:
							if (spec2 == 262)
								spec << "[Specialization]: Elemental";
							else if (spec2 == 263)
								spec << "[Specialization]: Enhancement";
							else if (spec2 == 264)
								spec << "[Specialization]: Restoration";
							break;
						case 8:
							if (spec2 == 62)
								spec << "[Specialization]: Arcane";
							else if (spec2 == 63)
								spec << "[Specialization]: Fire";
							else if (spec2 == 64)
								spec << "[Specialization]: Frost";
							break;
						case 9:
							if (spec2 == 265)
								spec << "[Specialization]: Affiliction";
							else if (spec2 == 266)
								spec << "[Specialization]: Demonology";
							else if (spec2 == 267)
								spec << "[Specialization]: Destruction";
							break;
						case 10:
							if (spec2 == 268)
								spec << "[Specialization]: Brewmaster";
							else if (spec2 == 270)
								spec << "[Specialization]: Mistweaver";
							else if (spec2 == 269)
								spec << "[Specialization]: Windwalker";
							break;
						case 11:
							if (spec2 == 102)
								spec << "[Specialization]: Balance";
							else if (spec2 == 103)
								spec << "[Specialization]: Feral";
							else if (spec2 == 104)
								spec << "[Specialization]: Guardian";
							else if (spec2 == 105)
								spec << "[Specialization]: Restoration";
							break;
						case 6:
							if (spec2 == 250)
								spec << "[Specialization]: Blood";
							else if (spec2 == 251)
								spec << "[Specialization]: Frost";
							else if (spec2 == 252)
								spec << "[Specialization]: Unholy";
							break;
						}
					}
				}
			}
		} while (result2->NextRow());
		if (spec.str() == "")
		{
			spec << "[Specialization]: UNKNOWN";
		}
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss0.str(), GOSSIP_SENDER_MAIN, 10000);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, spec.str(), GOSSIP_SENDER_MAIN, 10004);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss1.str(), GOSSIP_SENDER_MAIN, 10001);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss2.str(), GOSSIP_SENDER_MAIN, 10002);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ss3.str(), GOSSIP_SENDER_MAIN, 10003);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ilvl.str(), GOSSIP_SENDER_MAIN, 10005);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "[Invite]", GOSSIP_SENDER_MAIN, 100000 + playerguid);
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "[Back]", GOSSIP_SENDER_MAIN, -1000000);
		player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
		return true;
	}

	bool onGossipInvite(Player* player, Creature* creature, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();
		uint32 playerguid = action - 100000;
		Group* group = player->GetGroup();
		if (CharacterInfo const* characterInfo = sWorld->GetCharacterInfo(playerguid))
		{
			if (group)
			{
				if (Player* newPlayer = ObjectAccessor::FindPlayerByNameInOrOutOfWorld(characterInfo->Name.c_str()))
				{
					if (!(newPlayer == player))
					{
						if (!IsSameFaction(player, newPlayer))
						{
							ChatHandler(player->GetSession()).PSendSysMessage("|cffff6060[Information]:|r You are not the same faction as that player!|r!");
							return false;
						}
						group->AddInvite(newPlayer);
						newPlayer->GetSession()->SendGroupInviteNotification(newPlayer->GetName(), false);
						player->PlayerTalkClass->ClearMenus();
						onGossipListPlayerDetails(player, creature, action);
					}
				}
				else
				{
					ChatHandler(player->GetSession()).PSendSysMessage("ERROR: Player not found");
					player->PlayerTalkClass->ClearMenus();
					onGossipListPlayerDetails(player, creature, action);
				}

				return true;
			}
			else
			{
				if (Player* newPlayer = ObjectAccessor::FindPlayerByNameInOrOutOfWorld(characterInfo->Name.c_str()))
				{
					if (!(newPlayer == player))
					{
						group = new Group;
						group->Create(player);
						group->AddInvite(newPlayer);
						newPlayer->GetSession()->SendGroupInviteNotification(newPlayer->GetName(), false);
						player->PlayerTalkClass->ClearMenus();
						onGossipListPlayerDetails(player, creature, action);
					}
				}
				else
				{
					ChatHandler(player->GetSession()).PSendSysMessage("ERROR: Player not found");
					player->PlayerTalkClass->ClearMenus();
					onGossipListPlayerDetails(player, creature, action);
				}
				return true;
			}
		}
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
	{

		switch (action)
		{
		case 1:
			onGossipDPSOrHealer(player, creature);
			break;
		case 2:
			onGossipListGroups(player, creature);
			break;
		case 5:
			onGossipListGroupSignUps(player, creature);
			break;
		case 10:
			onGossipListPlayers(player, creature, false);
			break;
		case 11:
			onGossipListPlayers(player, creature, true);
		case -10000: 
			player->PlayerTalkClass->ClearMenus();
			onGossipListPlayers(player, creature, wantsHealers);
			break;
		case -10001: 
			player->PlayerTalkClass->ClearMenus();
			onGossipDPSOrHealer(player, creature);
			break;
		case -10002:
			player->PlayerTalkClass->ClearMenus();
			OnGossipHello(player, creature);
			break;
		case -10003:
			player->PlayerTalkClass->ClearMenus();
			OnGossipHello(player, creature);
			break;
		case -10004:
			player->PlayerTalkClass->ClearMenus();
			onGossipListGroups(player, creature);
			break;
		case -1000000:
			player->PlayerTalkClass->ClearMenus();
			onGossipListGroupSignUps(player, creature);
			break;
		}

		if (action >= 1000 && action <= 9999)
		{
			onGossipListPlayerDetails(player,creature,action);
			return true;
		}
		if (action <= -1000 && action >= -9999)
		{
			onGossipListGroupDetails(player, creature, action);
			return true;
		}
		if (action <= 10000 && action >= 99999)
		{
			onGossipSignUpForGroup(player, creature, action);
			return true;
		}
		if (action <= -100000 && action >= -999999)
		{
			onGossipListSignUpPlayerDetails(player, creature, action);
			return true;
		}
		if (action >= 100000 && action != -10000 && action != -10001 && action != -10002 && action != -10003 && action != -10004)
		{
			onGossipInvite(player, creature, action);
			return true;
		}
		return true;
	}

};

void AddSC_group_finder()
{
	new group_finder();
}