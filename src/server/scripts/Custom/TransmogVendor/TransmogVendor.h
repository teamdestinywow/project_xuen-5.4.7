
#pragma once

#include "Log.h"
#include "ScriptMgr.h"
#include "DatabaseEnv.h"

class TransmogVendor
{
public:
	static TransmogVendor* instance()
	{
		static TransmogVendor instance;
		return &instance;
	}

	void HandleCustomTransmog(Player* player, Creature* creature, uint32 vendorSlot, uint32 itemEntry)
	{
		if (ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(itemEntry))
		{
			player->CustomTransmog(player, itemEntry, player->CustomTransmogSelectedSlot);
		}
		else
			sLog->outInfo(LOG_FILTER_WORLDSERVER, "HandleCustomTransmog: itemTemplate for item %u is invalid", itemEntry);
	}

	bool HasItemInSlot(Player* player, uint8 slot)
	{
		if (player)
			if (Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, slot))
				return true;

		return false;
	}
	const char* GetSlotName(uint8 slot)
	{
		switch (slot)
		{
		case EQUIPMENT_SLOT_HEAD: return  "Head";
			case EQUIPMENT_SLOT_SHOULDERS: return  "Shoulders";
			case EQUIPMENT_SLOT_BODY: return  "Shirt";
			case EQUIPMENT_SLOT_CHEST: return  "Chest";
			case EQUIPMENT_SLOT_WAIST: return  "Waist";
			case EQUIPMENT_SLOT_LEGS: return  "Legs";
			case EQUIPMENT_SLOT_FEET: return  "Feet";
			case EQUIPMENT_SLOT_WRISTS: return  "Wrists";
			case EQUIPMENT_SLOT_HANDS: return  "Hands";
			case EQUIPMENT_SLOT_BACK: return  "Back";
			case EQUIPMENT_SLOT_MAINHAND: return  "Main hand";
			case EQUIPMENT_SLOT_OFFHAND: return  "Off hand";
			case EQUIPMENT_SLOT_RANGED: return  "Ranged";
			case EQUIPMENT_SLOT_TABARD: return  "Tabard";
			default: return nullptr;
		}
	}

	std::list<uint32> GetItemsBySlot(uint8 slot)
	{
		if (slot >= EQUIPMENT_SLOT_END || TransmogVendorContainer[slot].begin() == TransmogVendorContainer[slot].end())
			return {};
		
		return TransmogVendorContainer[slot];
	}
	void LoadFromDB()
	{
		TransmogVendorContainer.clear();

		uint32 msTime = getMSTime();
		QueryResult transmogvendor_data = WorldDatabase.Query("SELECT SlotId, ItemId FROM transmogvendor_data");

		uint32 count = 0;
		if (transmogvendor_data)
		{
			do
			{
				Field* fields = transmogvendor_data->Fetch();

				TransmogVendorContainer[fields[0].GetUInt8()].push_back(fields[1].GetInt32());
				++count;
			} while (transmogvendor_data->NextRow());
		}
		sLog->outInfo(LOG_FILTER_SERVER_LOADING, "Loaded %u transmogvendor item(s) in %u ms", count, GetMSTimeDiffToNow(msTime));
	}
private:
	std::unordered_map<uint8, std::list<uint32>> TransmogVendorContainer; // Slot Id, Item Entry
};
#define sTransmogVendor TransmogVendor::instance()

class TransmogVendorDisplay : public CreatureScript
{
public:
	TransmogVendorDisplay() : CreatureScript("TransmogVendorDisplay") {}

	enum Senders
	{
		SENDER_SELECT_VENDOR
	};

	bool OnGossipHello(Player* player, Creature* creature) override
	{
		player->PlayerTalkClass->ClearMenus();

		for (uint8 slot = EQUIPMENT_SLOT_START; slot < EQUIPMENT_SLOT_END; slot++)
		{
			if (sTransmogVendor->GetItemsBySlot(slot).empty())
				continue;

			if (const char* SlotName = sTransmogVendor->GetSlotName(slot))
				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, SlotName, SENDER_SELECT_VENDOR, slot);
		}

		if (!player->PlayerTalkClass->Empty())
			player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
		else
			creature->MonsterSay("Please come back later, at this time I have no items for use", LANG_UNIVERSAL, player->GetGUID());

		return true;
	}


	bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action) override
	{
		player->PlayerTalkClass->ClearMenus();
		WorldSession* session = player->GetSession();

		switch (sender)
		{
			case SENDER_SELECT_VENDOR:
			{
				std::vector<const ItemTemplate*> VendorItems;
				std::list<uint32> ItemList = sTransmogVendor->GetItemsBySlot(action);

				for (auto item : ItemList)
				{
					ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(item);

					if (!itemTemplate)
					{
						sLog->outInfo(LOG_FILTER_WORLDSERVER, "OnGossipSelect: itemTemplate for item %u is invalid", item);
						continue;
					}

					if (itemTemplate->InventoryType == INVTYPE_BAG ||
						itemTemplate->InventoryType == INVTYPE_RELIC ||
						itemTemplate->InventoryType == INVTYPE_BODY ||
						itemTemplate->InventoryType == INVTYPE_FINGER ||
						itemTemplate->InventoryType == INVTYPE_TRINKET ||
						itemTemplate->InventoryType == INVTYPE_AMMO ||
						itemTemplate->InventoryType == INVTYPE_QUIVER ||
						itemTemplate->InventoryType == INVTYPE_NON_EQUIP ||
						itemTemplate->InventoryType == INVTYPE_TABARD)
						continue;

					VendorItems.push_back(itemTemplate);
				}
				player->CLOSE_GOSSIP_MENU();

				if (!player->GetCreatureIfCanInteractWith(creature->GetGUID(), UNIT_NPC_FLAG_VENDOR))
				{
					sLog->outInfo(LOG_FILTER_WORLDSERVER, "WORLD: OnGossipSelect - Unit (GUID: %u) not found or you can not interact with him.", uint32(GUID_LOPART(creature->GetGUID())));
					player->SendSellError(SELL_ERR_CANT_FIND_VENDOR, NULL, 0);
					return true;
				}

				if (player->HasUnitState(UNIT_STATE_DIED))
					player->RemoveAurasByType(SPELL_AURA_FEIGN_DEATH);

				if (creature->HasUnitState(UNIT_STATE_MOVING))
					creature->StopMoving();

				player->CustomTransmogSelectedSlot = action;

				ByteBuffer itemsData(32 * VendorItems.size());
				uint32 count = 0;

				for (auto vendorItem : VendorItems)
				{
					if (count++ > MAX_VENDOR_ITEMS)
					{
						sLog->outInfo(LOG_FILTER_WORLDSERVER, "Count: %u exceeded MAX_VENDOR_ITEMS(500)", count);
						break;
					}

					itemsData << uint32(vendorItem->MaxDurability);
					itemsData << uint32(1); // Item Vendor Type (1 = Item, 2 = Currency)
					itemsData << uint32(vendorItem->BuyCount);
					itemsData << uint32(0); // Item Price
					itemsData << uint32(vendorItem->ItemId);
					itemsData << uint32(count); // Client expects counting to start at 1
					itemsData << int32(0xFFFFFFFF); // Left in stock (0xFFFFFFFF = Item will be colored, Positive Number = The amount of items left will be displayed, 0 = Item will be greyed out)
					itemsData << uint32(0);	// Unk UInt32
					itemsData << uint32(vendorItem->DisplayInfoID);
				}

				ObjectGuid guid = creature->GetGUID();
				WorldPacket data(SMSG_LIST_INVENTORY, 12 + itemsData.size());

				data.WriteBit(guid[4]);
				data.WriteBits(count, 18); // item count

				for (uint32 i = 0; i < count; i++)
				{
					data.WriteBit(false);	// unknown
					data.WriteBit(!false);	// has unknown
					data.WriteBit(true);	// has extended cost (True == false, False == true)
				}

				data.WriteBit(guid[1]);
				data.WriteBit(guid[6]);
				data.WriteBit(guid[2]);
				data.WriteBit(guid[5]);
				data.WriteBit(guid[7]);
				data.WriteBit(guid[0]);
				data.WriteBit(guid[3]);
				data.WriteByteSeq(guid[3]);
				data.append(itemsData);
				data.WriteByteSeq(guid[6]);
				data.WriteByteSeq(guid[0]);
				data.WriteByteSeq(guid[2]);
				data.WriteByteSeq(guid[5]);
				data << uint8(VendorItems.size());
				data.WriteByteSeq(guid[1]);
				data.WriteByteSeq(guid[4]);
				data.WriteByteSeq(guid[7]);

				session->SendPacket(&data);
			}
			break;
		}

		return true;
	}
};

class TransmogVendorLoader : public WorldScript
{
public:
	TransmogVendorLoader() : WorldScript("TransmogVendorLoader") {}

	void OnStartup() override
	{
		sTransmogVendor->LoadFromDB();
	}
};